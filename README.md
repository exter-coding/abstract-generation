# Abstract Generation

This project is meant to summarize the literature of the research papers based on the sections excluding its abstract and references and compare the summarized output with the original abstract to know the simarility.

## Documents:
- [Acute Lymphoblastic Leukemia Detection Using Hypercomplex-Valued Convolutional Neural Networks](https://arxiv.org/pdf/2205.13273.pdf) 
- [Basis-free Formulas for Characteristic Polynomial Coefficients in Geometric Algebras](https://arxiv.org/pdf/2205.13449.pdf)
- [Some equivalence relation between persistent homology and morphological dynamics](https://arxiv.org/pdf/2205.12546.pdf) 
- [Stability of the scattering transform for deformations with minimal regularity](https://arxiv.org/pdf/2205.11142.pdf)
- [What is an equivariant neural network?](https://arxiv.org/pdf/2205.07362.pdf) 
- [Convergence Analysis of Deep Residual Networks](https://arxiv.org/pdf/2205.06571.pdf)


## Get Started
```
git clone https://gitlab.com/exter-coding/abstract-generation.git
```

### Download the requirements:
```
pip install requirements.txt
```
### Go to scripts:
```
cd ./scripts
```
### Run the **`main.py`** file
```
python main.py
```
