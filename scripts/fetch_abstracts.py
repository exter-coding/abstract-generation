import requests as req
from bs4 import BeautifulSoup
from io import StringIO
import PyPDF2 as pdf
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfparser import PDFParser
from collections import Counter
import time
import warnings
warnings.filterwarnings("ignore")
### File Starts here ....

def getNames(fileName):
    #print(fileName)
    content=open(fileName,'r')
    inputNames=content.read().split('\n')
    return inputNames

def getResponses(inputNames,params):
    error={}
    responses=[]
    for name in inputNames:
      res=req.get('https://arxiv.org/search/?query='+name,params=params)
      if(res.status_code!=200):
        error.update({name:res.raise_for_status()})
      else:
        responses.append(res)
    return responses

def getAbstract(responses):
    abstract=[]
    for res in responses:
      soup = BeautifulSoup(res.text, features="html.parser")
      for script in soup(["script", "style"]):
        script.extract()
      text = soup.get_text()
      lines = (line.strip() for line in text.splitlines())
      
      chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
      
      text = '\n'.join(chunk for chunk in chunks if chunk)
      abstract.append(text[text.index("More")+4:text.index("Less")-2].replace('\n'," "))
    
    return abstract

def storeOutputs(names,abstract,loc):
    outputfile=open(loc,'w',encoding="utf-8")
    data=""
    for i in range(len(names)):
        data+=names[i]+" : "+abstract[i]+"\n"
    outputfile.write(data)


def remove_abstract(abstract,data,names,refs):
    output_string = StringIO()
    text=[]
    abss=[]
    removed=[]
    answer=dict()
    ms=[]
    for i in range(len(abstract)):
      time.sleep(2)
      with open(data+str(i)+'.pdf', 'rb') as in_file:
          parser = PDFParser(in_file)
          doc = PDFDocument(parser)
          rsrcmgr = PDFResourceManager()
          device = TextConverter(rsrcmgr, output_string, laparams=LAParams())
          interpreter = PDFPageInterpreter(rsrcmgr, device)
          for page in PDFPage.create_pages(doc):
              interpreter.process_page(page)
              ms.append(output_string.getvalue())
              output_string.truncate(0)
              output_string.seek(0)
      ms[refs[names[i]][1]+1]=refs[names[i]][0]
      ms=ms[:refs[names[i]][1]+2]
      #print(ms[refs[names[i]][1]+1])
      text=" ".join(ms).split()
      #print(text)
      abss=abstract[i].split()
      indices=[]
      matches=[]
      absIndexes=[]
      for word in range(len(abss)):
        try:
          if(text.index(abss[word])>=0):
            indices.append(abs(text.index(abss[word])-word))
            matches.append(text.index(abss[word]))
            absIndexes.append(word)
        except:
          pass
      map=Counter(indices)
      ans=sorted(map.items(), key =lambda kv:(kv[1], kv[0]))
      start=0
      m=0
      for x in range(len(indices)):
        if(indices[x]==ans[-1][0]):
            m=absIndexes[x]
            start=matches[x]
            break
      start=start-m
      end=start+len(abss)
      removed.append(text[start:end])
      answer.update({names[i]:text[end+5:]})
      #print(absIndexes[0])
      # absStart=text.index(txt[start])-m
      # return absStart+len(abstract)
    return removed,answer

            
    

        
      
def Remove_references(abstract,names):
  ans={}
  for paper in range(len(abstract)):
    time.sleep(2)
    pdf_object=open("../content/data"+str(paper)+".pdf","rb")
    pdf_result=pdf.PdfFileReader(pdf_object)
    NumberOfPages=pdf_result.numPages
    text=[]
    #print(pdf_result.getPage(8).extract_text())
    for i in range(1,NumberOfPages):
        getPage=pdf_result.getPage(i)
        text.append(getPage.extract_text().replace("\n"," "))
    f=0
    
    for i in range(len(text)-1,-1,-1):
      x=text[i].lower().split()
    
      try:
        
        
        if(x.index("references")>=0):
          f=x.index("references")
          ans.update({names[paper]:[" ".join(x[:f]),i]})
          #print(text[i])
          break
      except:
        pass
  return ans
  



